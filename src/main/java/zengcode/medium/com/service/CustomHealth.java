package zengcode.medium.com.service;

import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

import java.util.Random;

@Component
public class CustomHealth implements HealthIndicator {

    @Override
    public Health health() {

        int systemStatusCode = 2;

        if (systemStatusCode == 1) {
            return Health.up().build();
        }

        if (systemStatusCode == 2) {
            return Health.outOfService().build();
        }

        return Health.down().withDetail("Error : ", systemStatusCode).build();
    }

    /**
     * Implement logic to checking your system health
     * @return int
     */
    private int generateSystemStatusCode() {
        Random rand = new Random();
        return rand.nextInt(3) + 1;
    }

}